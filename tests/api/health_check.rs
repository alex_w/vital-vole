use crate::helpers::TestApp;

#[tokio::test]
async fn health_check_returns_ok() {
    // Arrange
    let app = TestApp::spawn();

    // Act
    let response = app.get_health_check().await;

    // Assert
    assert!(response.status().is_success());
    assert_eq!(Some(0), response.content_length());
}
