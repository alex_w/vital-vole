use vital_vole::startup::startup;

#[tokio::main]
async fn main() {
    let port = 8080;
    let (_address, run) = startup(port);

    run.await
}
