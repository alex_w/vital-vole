use reqwest::{Client, Url};
use vital_vole::startup::startup;

pub struct TestApp {
    client: reqwest::Client,
    address: Url,
}

impl TestApp {
    pub fn spawn() -> Self {
        let client = Client::new();

        let port = 0;
        let (address, run) = startup(port);
        let address = format!("http://{address}");
        let address = Url::parse(&address).expect("Failed to parse address.");
        tokio::spawn(run);

        Self { client, address }
    }

    pub async fn get_health_check(&self) -> reqwest::Response {
        self.client
            .get(self.address("health-check"))
            .send()
            .await
            .expect("Failed to execute request.")
    }

    pub fn address(&self, path: &str) -> Url {
        self.address.join(path).expect("Failed to join address.")
    }
}
