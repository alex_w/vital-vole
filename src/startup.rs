use core::future::Future;
use std::net::SocketAddr;
use warp::Filter;

pub fn startup(port: u16) -> (SocketAddr, impl Future<Output = ()>) {
    let address = ([0, 0, 0, 0], port);

    let filters = warp::path!("health-check")
        .and(warp::get())
        .then(health_check);

    warp::serve(filters).bind_ephemeral(address)
}

async fn health_check() -> impl warp::Reply {
    warp::reply()
}
